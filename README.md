This project is now DEPRECATED.
Please refer to resttest2 instead.

# RestTest

The base path for all API operations is <hostname>/RestTest/rest-test/offers.

The REST API comprises the following operations and paths:

1) Add a new offer - POST using /post. The contents of the offer will be in the body of the post, e.g: { "name": "woogle", "description": "Only for an hour!!!!", "priceAmt": "99.00", "currency" : "GBP", "hours" : 1 }

2) Query all offers - GET request with no suffix

3) Query a single offer by mame - GET request suffixed by /<name>

4) Cancel an offer (expire with immediate effect) - PUT request suffixed by /expire/<name>

Comments: There are a number of enhamcements I would make, and also things I would change based on this experience: 

- In an enterprise application, I would use a framework (e.g. Spring) to permit dependency injection. This would also enable the use of mock injection in the OfferServiceTest class 

- I would also use a logging framework (e.g. Log4J) and introduce an appropriate level of logging 

- I have used several older libraries due to the coonstraints of my current environment. Best practice would be to use the latest stable versions for security. This would enable the 
use of annotations such as @Consumes, @Produces, that are not supported in earlier versions. 

- It might have been good to use Spring Boot!
