package com.mytest.resttest.datalayer;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.mytest.resttest.offer.Offer;
import com.mytest.resttest.offer.OfferUtils;

public class OfferDAOImplTest {
    
    private static final int ANY_DURATION = 5;
    private static final String ANY_CURRENCY = "GBP";
    private static final String ANY_PRICE = "10.00";
    private static final String ANY_DESCRIPTION = "Any description";
    private static final String ANY_FILE_NAME = "Any name";
    private static final String ANY_OTHER_FILE_NAME = "A different name";
    private static final String ANY_OTHER_DESCRIPTION = "A different description";
    private static final String ANY_OTHER_PRICE = "15.00";
    private static final String ANY_OTHER_CURRENCY = "EUR";
    private static final String A_FILE_TO_BE_EXPIRED = "A file that we will expire";
    
    // In a more sophisticated application, we would inject this
    OfferDAO dao = new OfferDAOImpl();

    @Before
    public void tearDown() throws IOException {
        FileUtils.cleanDirectory(OfferUtils.DATASTORE_DIRECTORY); 
    }
    
    @AfterClass
    public static void tearDownAtEnd() throws IOException {
        FileUtils.cleanDirectory(OfferUtils.DATASTORE_DIRECTORY); 
    }
    
    @Test
    public void whenNewOfferAdded_thenTotalFilesAreIncremented() throws IOException {
        Offer offer = new Offer(ANY_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(offer);
        
        assertEquals(1, countOffers());
        
        Offer different_offer = new Offer(ANY_OTHER_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(different_offer);
        
        assertEquals(2, countOffers());
    }

    @Test
    public void whenDuplicateNameUsed_thenNoFilesAreCreated() throws IOException {
        Offer offer = new Offer(ANY_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(offer);
        
        assertEquals(1, countOffers());
        
        Offer different_offer = new Offer(ANY_FILE_NAME, ANY_OTHER_DESCRIPTION, new BigDecimal(ANY_OTHER_PRICE), ANY_OTHER_CURRENCY, ANY_DURATION);
        dao.addOffer(different_offer);
        
        assertEquals(1, countOffers());
    } 
    
    @Test
    public void whenFetchedByName_thenOfferIsReturned() throws IOException {
        Offer offer = new Offer(ANY_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(offer);
        
        Offer fetchedOffer = dao.fetchOffer(ANY_FILE_NAME);
        
        assertEquals(offer.getName(), fetchedOffer.getName());
        assertEquals(offer.getDescription(), fetchedOffer.getDescription());
        assertEquals(offer.getPriceAmt(), fetchedOffer.getPriceAmt());
        assertEquals(offer.getCurrency(), fetchedOffer.getCurrency());
        assertEquals(offer.getHours(), fetchedOffer.getHours());
        // Note start and end time are equal even though they are updated by the DAO
        assertEquals(offer.getStartTime(), fetchedOffer.getStartTime());
        assertEquals(offer.getEndTime(), fetchedOffer.getEndTime());
    } 
   
    @Test
    public void whenFileExists_thenIfExistsReturnsTrue() throws IOException {
        Offer offer = new Offer(ANY_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(offer);
        
        assertEquals(true, dao.ifExists(offer));
        assertEquals(true, dao.ifExists(ANY_FILE_NAME));
    }     
    
    @Test
    public void whenFileDoesNotExist_thenIfExistsReturnsFalse() throws IOException {
        Offer offer = new Offer(ANY_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        // Do not persist !!!
        
        assertEquals(false, dao.ifExists(offer));
        assertEquals(false, dao.ifExists(ANY_FILE_NAME));
    }      
    
    @Test
    public void whenFetchingAllOffers_thenAllNonExpiredOffersAreReturned() throws IOException {
        Offer offer = new Offer(ANY_FILE_NAME, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(offer);
        Offer different_offer = new Offer(ANY_OTHER_FILE_NAME, ANY_OTHER_DESCRIPTION, new BigDecimal(ANY_OTHER_PRICE), ANY_OTHER_CURRENCY, ANY_DURATION);
        dao.addOffer(different_offer);
        Offer expiry_offer = new Offer(A_FILE_TO_BE_EXPIRED, ANY_OTHER_DESCRIPTION, new BigDecimal(ANY_OTHER_PRICE), ANY_OTHER_CURRENCY, ANY_DURATION);
        dao.addOffer(expiry_offer);
        
        List<Offer> allOffersNoneExpired = dao.fetchLiveOffers();
        assertEquals(allOffersNoneExpired.size(), 3);
        
        dao.expireOffer(A_FILE_TO_BE_EXPIRED);
        List<Offer> allOffersExcludingExpired = dao.fetchLiveOffers();
        assertEquals(allOffersExcludingExpired.size(), 2);
     } 
    
    @Test
    public void whenOfferIsExpired_thenEndDateIsSetToNow() throws IOException {
        Offer offer = new Offer(A_FILE_TO_BE_EXPIRED, ANY_DESCRIPTION, new BigDecimal(ANY_PRICE), ANY_CURRENCY, ANY_DURATION);
        dao.addOffer(offer);
        // A future expiry date should always be set
        assertTrue(offer.getEndTime().isAfter(LocalDateTime.now()));
        
        dao.expireOffer(A_FILE_TO_BE_EXPIRED);
        Offer expiredOffer = dao.fetchOffer(A_FILE_TO_BE_EXPIRED);
        // Expiry date will be before or (possibly) equal to now
        assertFalse(expiredOffer.getEndTime().isAfter(LocalDateTime.now()));
     }     
    
    
    
    private long countOffers() throws IOException {
        long count;
        try (Stream<Path> files = Files.list(Paths.get(OfferUtils.DATASTORE_PATH))) {
            count = files.count();
        }
        return count;
    }

}
