package com.mytest.resttest.service;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.mytest.resttest.offer.OfferUtils;
public class OfferServiceTest {
    

    OfferService offerService = new OfferService();
    String fooOffer = 
            "{ \"name\": \"foo\", \"description\": \"Only for an hour!!!!\", \"priceAmt\": \"99.00\", \"currency\" : \"GBP\", \"hours\" : 1 }";
    String barOffer = 
            "{ \"name\": \"bar\", \"description\": \"Only for 10 hours!!!!\", \"priceAmt\": \"150.00\", \"currency\" : \"GBP\", \"hours\" : 10 }";
    

    @Before
    public void tearDown() throws IOException {
        FileUtils.cleanDirectory(OfferUtils.DATASTORE_DIRECTORY); 
    }
    
    @AfterClass
    public static void tearDownAtEnd() throws IOException {
        FileUtils.cleanDirectory(OfferUtils.DATASTORE_DIRECTORY); 
    }

    @Test
    public void whenAddingValidNew_returnSuccessResponseWithSuccessMessage() {
        Response response = offerService.newOffer(fooOffer);
        assertTrue(response.getStatus() == 200);
        assertTrue(response.getEntity().toString().contains("The following offer was added:"));
    }

    @Test
    public void whenAddingDuplicateNew_returnSuccessResponseWithRejectionMessage() {
        Response response = offerService.newOffer(fooOffer);
        response = offerService.newOffer(fooOffer);
        assertTrue(response.getStatus() == 200);
        assertTrue(response.getEntity().toString().contains("An offer already exists with this name:"));
    }

    @Test
    public void whenFetchingAll_returnAllNonExpired() {
        offerService.newOffer(fooOffer);
        offerService.newOffer(barOffer);
        
        Response response = offerService.fetchAllOffers();
        assertTrue(response.getStatus() == 200);
        assertTrue(response.getEntity().toString().contains("The following offers are currently available:"));
        String body = response.getEntity().toString();
        assertTrue(body.contains("foo"));
        assertTrue(body.contains("bar"));
        
        offerService.expireOffer("foo");
        
        Response newResponse = offerService.fetchAllOffers();
        assertTrue(newResponse.getStatus() == 200);
        assertTrue(newResponse.getEntity().toString().contains("The following offers are currently available:"));
        String newBody = newResponse.getEntity().toString();
        assertTrue(!newBody.contains("foo"));
        assertTrue(newBody.contains("bar"));
    }

    @Test
    public void whenFetchingSingle_returnWhetherExpiredOrNot() {
        offerService.newOffer(fooOffer);
        offerService.newOffer(barOffer);
        
        Response response = offerService.fetchOffer("foo");
        assertTrue(response.getStatus() == 200);
        assertTrue(response.getEntity().toString().contains("The offer details are:"));
        String body = response.getEntity().toString();
        assertTrue(body.contains("foo"));
        assertTrue(!body.contains("bar"));
        
        offerService.expireOffer("foo");
        
        Response newResponse = offerService.fetchOffer("foo");
        assertTrue(newResponse.getStatus() == 200);
        assertTrue(newResponse.getEntity().toString().contains("The offer details are:"));
        String newBody = newResponse.getEntity().toString();
        assertTrue(newBody.contains("foo"));
    }
    
    // Plus could test for failure cases, exceptions, etc

}
