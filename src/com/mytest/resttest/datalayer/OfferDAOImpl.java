package com.mytest.resttest.datalayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.mytest.resttest.offer.Offer;
import com.mytest.resttest.offer.OfferUtils;


public class OfferDAOImpl implements OfferDAO {
    
    @Override
    public Offer addOffer(Offer offer) throws FileNotFoundException {
       
        LocalDateTime startTime = LocalDateTime.now();
        LocalDateTime endTime = startTime.plus(offer.getHours(), ChronoUnit.HOURS);// TODO Days? Years? Minutes?
        offer.setStartTime(startTime);
        offer.setEndTime(endTime);
        String json = OfferUtils.toJson(offer);
        try (PrintWriter out = new PrintWriter(buildFileName(offer.getName()))) {
            out.println(json);
        } 
        return offer;
     }

    @Override
    public boolean ifExists(Offer offer) {
        return ifExists(offer.getName());
    }

    @Override
    public boolean ifExists(String offerName) {
        File thisFile = new File (buildFileName(offerName));
        return thisFile.exists();
    }

    private String buildFileName(String offerName) {
        return OfferUtils.DATASTORE_PATH + offerName + ".txt";
    }

    @Override
    public List<Offer> fetchLiveOffers() throws IOException {
        List<Offer> allOffers = new ArrayList<>();
        for (final File fileEntry : OfferUtils.DATASTORE_DIRECTORY.listFiles()) {
            if (fileEntry.isDirectory()) {
                // ignore
            } else {
                try (BufferedReader br = new BufferedReader(new FileReader(fileEntry))) {
                    String currentLine;
                    StringBuffer sb = new StringBuffer();
                    while ((currentLine = br.readLine()) != null) {
                        sb.append(currentLine);
                    }
                    Offer thisOffer = OfferUtils.toOffer(sb.toString());
                    if (thisOffer.isLive()) {
                        allOffers.add(thisOffer);
                    }
                } 
            }
        }        
        return allOffers;
    }

    @Override
    public Offer fetchOffer(String name) throws IOException {
        Offer thisOffer = null;
        try (BufferedReader br = new BufferedReader(new FileReader(buildFileName(name)))){
            String currentLine;
            StringBuffer sb = new StringBuffer();
            while ((currentLine = br.readLine()) != null) {
                sb.append(currentLine);
            }
            thisOffer = OfferUtils.toOffer(sb.toString());
        } 
        return thisOffer;
    }

    @Override
    public Offer expireOffer(String name) throws IOException {
        Offer thisOffer = fetchOffer(name);
        if (null != thisOffer) {
            thisOffer.setEndTime(LocalDateTime.now());
            String json = OfferUtils.toJson(thisOffer);
            try (PrintWriter out = new PrintWriter(buildFileName(thisOffer.getName()))) {
                out.println(json);
            } 
         }
        return thisOffer;
    }

}
