package com.mytest.resttest.datalayer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.mytest.resttest.offer.Offer;

public interface OfferDAO {
    // We can replace file-based implementation with a DB-based implementation without
    // changing the calling code
    public Offer addOffer(Offer offer) throws FileNotFoundException;
    boolean ifExists(String offerName);   
    public boolean ifExists(Offer offer);
    public List<Offer> fetchLiveOffers() throws IOException;
    public Offer fetchOffer(String name) throws FileNotFoundException, IOException;
    public Offer expireOffer(String name) throws IOException;
}
