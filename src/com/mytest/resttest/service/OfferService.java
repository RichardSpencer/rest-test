package com.mytest.resttest.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.JsonSyntaxException;
import com.mytest.resttest.datalayer.OfferDAO;
import com.mytest.resttest.datalayer.OfferDAOImpl;
import com.mytest.resttest.offer.Offer;
import com.mytest.resttest.offer.OfferUtils;


@Path("/offers")
public class OfferService {

    // In a fully-developed solution, would put these in a config file
    private static final String SUCCESS_ALL_VALID = "The following offers are currently available:";
    private static final String SUCCESS_FETCH = "The offer details are:";
    private static final String SUCCESS_ADD = "The following offer was added:";
    private static final String SUCCESS_EXPIRE = "The following offer has been cancelled:";
    private static final String FAIL_GENERIC = "Failed to add the following offer. The cause may be incorrect input data or a server error";
    private static final String FAIL_OFFER_EXISTS = "An offer already exists with this name:";
    private static final String FAIL_NOT_FOUND = "No offer found with this name";

    OfferDAO offerDao = new OfferDAOImpl(); // In an enterprise solution, this would be injected
    
    @POST
    @Path("/post")
    public Response newOffer(String input) {
        
        Offer offer = null;
        try {
            offer = OfferUtils.toOffer(input);
            // we could also validate the input for completeness here
            if (! offerDao.ifExists(offer)) {
               offerDao.addOffer(offer); 
            } else {
                return buildResponse(FAIL_OFFER_EXISTS, offer, Response.Status.OK);
            }
        } catch (JsonSyntaxException | IOException ex ) {
            return buildResponse(FAIL_GENERIC, offer, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return buildResponse(SUCCESS_ADD, offer, Response.Status.OK);
    }

    @GET
    public Response fetchAllOffers() {
        List<Offer> allOffers = new ArrayList<>();;
        try {
            allOffers = offerDao.fetchLiveOffers();
        } catch (IOException e) {
            return buildResponse(FAIL_GENERIC, allOffers, Response.Status.INTERNAL_SERVER_ERROR);
        }
       return buildResponse(SUCCESS_ALL_VALID, allOffers, Response.Status.OK);
    }
   
    @GET
    @Path("/{offerName}")
    public Response fetchOffer(@PathParam("offerName") String name) {
        
        Offer thisOffer = null;
        try {
            if (offerDao.ifExists(name)) {
                thisOffer = offerDao.fetchOffer(name);
            } else {
                return buildResponse(FAIL_NOT_FOUND, thisOffer, Response.Status.OK);
            }
        } catch (Exception e) {
            return buildResponse(FAIL_GENERIC, thisOffer, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return buildResponse(SUCCESS_FETCH, thisOffer, Response.Status.OK);
    }

    @PUT
    @Path("expire/{offerName}")
    public Response expireOffer(@PathParam("offerName") String name) {
        
        Offer thisOffer = null;
        try {
            if (offerDao.ifExists(name)) {
                thisOffer = offerDao.expireOffer(name);
            } else {
                return buildResponse(FAIL_NOT_FOUND, thisOffer, Response.Status.OK);
            }
        } catch (Exception e) {
            return buildResponse(FAIL_GENERIC, thisOffer, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return buildResponse(SUCCESS_EXPIRE, thisOffer, Response.Status.OK);
    }
    
    private Response buildResponse(String message, Offer offer, Status status) {
        List<Offer> offers = Stream.of(offer).collect(Collectors.toList());
        return buildResponse(message, offers, status);
    }
    
    private Response buildResponse(String message, List<Offer> offers, Status status) {
        ResponseEntity resp = new ResponseEntity(message, offers);
        String respJson = OfferUtils.toJson(resp);
        return Response.status(status).entity(respJson).build();
    }
    
    
    @SuppressWarnings("unused")
    private class ResponseEntity {
        
        //NOTE: The class doesn't need getters because Gson uses reflection
        
		final String message; //NOPMD
        final List<Offer> offers; //NOPMD
        
        ResponseEntity (String message, List<Offer> offers) {
            this.message = message;
            this.offers = offers;
        }

    }
}
