package com.mytest.resttest.offer;

import java.io.File;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OfferUtils {
    
    public final static String DATASTORE_PATH = "datastore" + File.separator;
    public final static File DATASTORE_DIRECTORY = new File(DATASTORE_PATH);
    

    public static String toJson(Object entity) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz")
                .setPrettyPrinting()
                .create();
        String result = gson.toJson(entity);
        return result.replace("\\\"", "");
    }
    
    public static Offer toOffer(String input) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz")
                .setPrettyPrinting()
                .create();
        Offer result = gson.fromJson(input, Offer.class);
        return result;
    }

}
