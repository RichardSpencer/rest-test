package com.mytest.resttest.offer;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Offer {
    
    private final String name;
    private final String description;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private final BigDecimal priceAmt;
    private final String currency;
    private final int hours;
    
    public Offer (String name, String description, BigDecimal priceAmt, String currency, Integer hours) {
        this.name = name;
        this.description = description;
        this.priceAmt = priceAmt;
        this.currency = currency;
        this.hours = hours;
    }
    
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }
    
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }
    
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getPriceAmt() {
        return priceAmt;
    }
    
    public String getCurrency() {
        return currency;
    }
    
    public int getHours() {
        return hours;
    }
    
    public boolean isLive() {
        return this.endTime == null || this.endTime.isAfter(LocalDateTime.now()) ? true : false;
    }
     
    public String toString() {
        StringBuffer sb = new StringBuffer("Offer: ");
        sb.append(getName());
        sb.append(":");
        sb.append(getDescription());
        sb.append(":");
        sb.append(getPriceAmt().toString());
        sb.append(":");
        sb.append(getCurrency());
        return sb.toString();
    }
    
}
